

import os


import time
import numpy as np
from pycocotools.coco import COCO
import mindspore.common.dtype as mstype
from mindspore import context
from mindspore.train.serialization import load_checkpoint, load_param_into_net,save_checkpoint
from mindspore.common import set_seed, Parameter
from mindspore import Tensor

import torch

key_mapping = {'RCNN_base': 'backbone.layers',
               'RCNN_rpn.RPN_Conv': 'rpn_with_loss.rpn_convs_list.0.rpn_conv',
               'RCNN_rpn.RPN_cls_score': 'rpn_with_loss.rpn_convs_list.0.rpn_cls',
               'RCNN_rpn.RPN_bbox_pred': 'rpn_with_loss.rpn_convs_list.0.rpn_reg',
               'RCNN_top': 'backbone.classifier',
               'RCNN_cls_score': 'rcnn.cls_scores',
               'RCNN_bbox_pred': 'rcnn.reg_scores',
               }

ms_ckpt_path = "/home/zy/Huaweicloud/faster_rcnn/ckpt_0/faster_rcnn-4_66.ckpt"
ms_ckpt_init = "/home/zy/Huaweicloud/faster_rcnn/checkpoints/frcnn_init_withbiasnormal.ckpt"
pt_ckpt_path = "/home/zy/PycharmProjects/faster-rcnn.pytorch/models/vgg16/pascal_voc/faster_rcnn_1_9_2504.pth"
ms_from_pt = "/home/zy/Huaweicloud/faster_rcnn/checkpoints/pt2ms_frcnn_update_name.ckpt"
# checkpoint = torch.load(pt_ckpt_path)
# #print(checkpoint)
# state_dict = []
# for key, value in checkpoint['model'].items():
#     print(key,value.size())
#     value_np = value.cpu().numpy()
#
#     value_ms = Tensor(value_np)
#     for k, v in key_mapping.items():
#         if k in key:
#             key = key.replace(k, v)
#             break
#     state_dict.append({"name":key, "data": value_ms})
# save_checkpoint(state_dict, ms_from_pt)


param_dict = load_checkpoint(ms_ckpt_path)
ms_from_pt_dict = load_checkpoint(ms_from_pt)

for key, value in param_dict.items():
    print(key)
    tensor = value.asnumpy().astype(np.float32)
    # print(tensor)
    param_dict[key] = Parameter(tensor, key)
